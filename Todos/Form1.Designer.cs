﻿namespace Todos {
    partial class FormMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lbZadaci = new System.Windows.Forms.CheckedListBox();
            this.btnUnesiZadatak = new System.Windows.Forms.Button();
            this.tbZadatak = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbZadaci
            // 
            this.lbZadaci.FormattingEnabled = true;
            this.lbZadaci.Location = new System.Drawing.Point(12, 12);
            this.lbZadaci.Name = "lbZadaci";
            this.lbZadaci.Size = new System.Drawing.Size(280, 244);
            this.lbZadaci.TabIndex = 1;
            this.lbZadaci.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lbZadaci_ItemCheck);
            this.lbZadaci.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lbZadaci_KeyDown);
            // 
            // btnUnesiZadatak
            // 
            this.btnUnesiZadatak.Location = new System.Drawing.Point(12, 314);
            this.btnUnesiZadatak.Name = "btnUnesiZadatak";
            this.btnUnesiZadatak.Size = new System.Drawing.Size(280, 33);
            this.btnUnesiZadatak.TabIndex = 2;
            this.btnUnesiZadatak.Text = "Dodaj zadatak";
            this.btnUnesiZadatak.UseVisualStyleBackColor = true;
            this.btnUnesiZadatak.Click += new System.EventHandler(this.btnUnesiZadatak_Click);
            // 
            // tbZadatak
            // 
            this.tbZadatak.Location = new System.Drawing.Point(12, 273);
            this.tbZadatak.Name = "tbZadatak";
            this.tbZadatak.Size = new System.Drawing.Size(280, 20);
            this.tbZadatak.TabIndex = 3;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 368);
            this.Controls.Add(this.tbZadatak);
            this.Controls.Add(this.btnUnesiZadatak);
            this.Controls.Add(this.lbZadaci);
            this.Name = "FormMain";
            this.Text = "Todos";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox lbZadaci;
        private System.Windows.Forms.Button btnUnesiZadatak;
        private System.Windows.Forms.TextBox tbZadatak;
    }
}

