﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Todos {
    public partial class FormMain : Form {
        public FormMain() {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e) {
            OsvjeziListuZadataka();
        }

        private void OsvjeziListuZadataka() {
            var zadaci = Zadaci.DohvatiZadatke();
            lbZadaci.Items.Clear();
            foreach (var zadatak in zadaci) {
                lbZadaci.Items.Add(zadatak, zadatak.Zavrseno);
            }
        }

        // FormMain.cs
        private void btnUnesiZadatak_Click(object sender, EventArgs e) {
            var zadatak = new Zadatak(tbZadatak.Text, false);
            if (!Zadaci.UnesiZadatak(zadatak)) {
                MessageBox.Show("Doslo je do greške prilikom unosa zadatka!",
                    "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            OsvjeziListuZadataka();
        }

        // FormMain.cs
        private void lbZadaci_ItemCheck(object sender, ItemCheckEventArgs e) {
            var zadatak = (Zadatak)lbZadaci.Items[e.Index];
            if (e.NewValue == CheckState.Checked) {
                zadatak.Zavrseno = true;
            } else {
                zadatak.Zavrseno = false;
            }
            Zadaci.AzurirajZadatak(zadatak);
        }

        // FormMain.cs
        private void lbZadaci_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Delete) {
                if (lbZadaci.SelectedIndex != -1) {
                    var zadatak = (Zadatak)lbZadaci.Items[lbZadaci.SelectedIndex];
                    Zadaci.ObrisiZadatak(zadatak);
                    OsvjeziListuZadataka();
                }
            }
        }
    }
}
