﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;

// Baza.cs
namespace Todos {
    static class Baza {
        public static string KonekcijskiString() {
            return string.Format(
                "Data Source = {0}; Initial Catalog = {1}; User ID = {2}; Password = {3}",
                server,
                baza,
                korisnickoIme,
                lozinka
            );
        }

        private const string server = "";
        private const string baza = "todos";
        private const string korisnickoIme = "sa";
        private const string lozinka = "";
    }
}
